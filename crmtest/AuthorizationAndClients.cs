﻿using System;
using System.Net.Mime;
//using crmtest.constants;
using Constants;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using crmtest.DriverHelper;
using System.Threading;


namespace crmtest
{
   // [TestClass]

    public class UnitTest1
    {


        [Test, Timeout(30000)]
        [TestCase(CrmConstants.validLogin, CrmConstants.validPassword)]
        [TestCase(CrmConstants.validLogin2, CrmConstants.validPassword2)]

        public void AuthorizationTrue(string login, string password)//корректная авторизация
        {
            IWebDriver driver = new ChromeDriver();
            try
            {
                {
                    driver.Navigate().GoToUrl(CrmConstants.addressDev);
                    HelpMethods.FindElement(driver, By.Id("input_0"), 1).SendKeys(login); //поиск строки для ввода логина
                    HelpMethods.FindElement(driver, By.Id("input_1"), 1).SendKeys(password); //поиск строки для ввода пароля
                    HelpMethods.FindElement(driver, By.ClassName("md-raised"), 1).Submit(); //поиск кнопки
                    var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                    IWebElement waitTable =
                        wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                    Assert.IsTrue(waitTable.Displayed);
                    driver.Close();
                    driver.Quit();

                }
            }
            catch (Exception e)
            {

                driver.Close();
                driver.Quit();
                throw;
            }

        }

        [Test, Timeout(30000)]
        [TestCase("abc", "abc")]
        [TestCase("123", "123")]
        public void AuthorizationFalse(string login, string password)//авторизация с ошибкой
        {
            IWebDriver driver = new ChromeDriver();
            try
            {
                {
                    driver.Navigate().GoToUrl(CrmConstants.addressDev);
                    IWebElement element =
                        HelpMethods.FindElement(driver, By.Id("input_0"), 1); //поиск строки для ввода логина
                    element.SendKeys(login); //логин
                    IWebElement element2 =
                        HelpMethods.FindElement(driver, By.Id("input_1"), 1); //поиск строки для ввода пароля
                    element2.SendKeys(password); //пароль
                    IWebElement element3 = HelpMethods.FindElement(driver, By.ClassName("md-raised"), 1); //поиск кнопки
                    element3.Submit(); //нажатие на кнопку
                    var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                    IWebElement waitTable = wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("md-subhead")));
                    Assert.IsTrue(waitTable.Displayed);
                    driver.Close();
                    driver.Quit();

                }
            }
            catch (Exception e)
            {

                driver.Close();
                driver.Quit();
                throw;
            }
        }

        [Test]
        public void SearchByCode()//поиск по коду клиента
        {
            IWebDriver driver = new ChromeDriver();
            try
            {

                HelpMethods.Authorization(CrmConstants.validLogin, CrmConstants.validPassword, driver);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                driver.Navigate().Refresh();
                IWebElement SearchCode = HelpMethods.FindElement(driver, By.Id("input_24"), 4);
                SearchCode.SendKeys("ТестМобСайт2");
                SearchCode.SendKeys(Keys.Enter);
                IWebElement findElement = HelpMethods.FindElement(driver, By.LinkText("ТестМобСайт2"), 5);
                Assert.IsTrue(findElement.Displayed);
                driver.Close();
                driver.Quit();
            }
            catch (Exception e)
            {
                driver.Close();
                driver.Quit();
                throw;
            }
        }

        [Test]
        public void SearchByNumber()//поиск по номеру клиента
        {
            IWebDriver driver = new ChromeDriver();
            try
            {

                HelpMethods.Authorization(CrmConstants.validLogin, CrmConstants.validPassword, driver);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                driver.Navigate().Refresh();
                IWebElement searchElement = HelpMethods.FindElement(driver, By.Id("input_24"), 4);
                searchElement.SendKeys("1030");
                searchElement.SendKeys(Keys.Enter);
                IWebElement findElement = HelpMethods.FindElement(driver, By.LinkText("ТестМобСайт2"), 5);
                Assert.IsTrue(findElement.Displayed);
                driver.Close();
                driver.Quit();
            }
            catch (Exception e)
            {
                driver.Close();
                driver.Quit();
                throw;
            }
        }

        [Test]
        public void SearchByIncorrectNumber()//поиск по некорректному номеру или коду клиента
        {
            IWebDriver driver = new ChromeDriver();
            try
            {

                HelpMethods.Authorization(CrmConstants.validLogin, CrmConstants.validPassword, driver);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                driver.Navigate().Refresh();
                IWebElement searchElement = HelpMethods.FindElement(driver, By.Id("input_24"), 4);
                searchElement.SendKeys("50000");
                searchElement.SendKeys(Keys.Enter);
                string Text = "По Вашему запросу ничего не найдено";
                Thread.Sleep(5000);
                var elementText = HelpMethods.FindElementWithText(driver, By.ClassName("table__cell"), Text, 3);

                Assert.IsTrue(elementText.Displayed);
                driver.Close();
                driver.Quit();
            }
            catch (Exception e)
            {
                driver.Close();
                driver.Quit();
                throw;
            }
        }



        [Test]
        public void SearchByTelephoneNumber()//поиск по телефонному номеру
        {
            IWebDriver driver = new ChromeDriver();
            try
            {

                HelpMethods.Authorization(CrmConstants.validLogin, CrmConstants.validPassword, driver);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                driver.Navigate().Refresh();
                IWebElement searchPhoneElement = HelpMethods.FindElement(driver, By.Id("input_25"), 4);
                searchPhoneElement.SendKeys("9650794097");
                searchPhoneElement.SendKeys(Keys.Enter);
                IWebElement findElement = HelpMethods.FindElement(driver, By.LinkText("тополь"), 4);
                Assert.IsTrue(findElement.Displayed);
                driver.Close();
                driver.Quit();
            }
            catch (Exception e)
            {
                driver.Close();
                driver.Quit();
                throw;
            }
        }

        [Test]
        public void SearchByIncorrectTelephoneNumber()//поиск по некорректному телефонному номеру
        {
            IWebDriver driver = new ChromeDriver();
            try
            {

                HelpMethods.Authorization(CrmConstants.validLogin, CrmConstants.validPassword, driver);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                driver.Navigate().Refresh();
                IWebElement element = HelpMethods.FindElement(driver, By.Id("input_25"), 4);
                element.SendKeys("5000423420");
                element.SendKeys(Keys.Enter);
                string Text = "По Вашему запросу ничего не найдено";
                Thread.Sleep(5000);
                var elementText = HelpMethods.FindElementWithText(driver, By.ClassName("table__cell"), Text, 3);

                Assert.IsTrue(elementText.Displayed);
                driver.Close();
                driver.Quit();
            }
            catch (Exception e)
            {
                driver.Close();
                driver.Quit();
                throw;
            }
        }




        [Test]
        public void SearchByEmail()//поиск по email
        {
            IWebDriver driver = new ChromeDriver();
            try
            {

                HelpMethods.Authorization(CrmConstants.validLogin, CrmConstants.validPassword, driver);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                driver.Navigate().Refresh();
                //wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                IWebElement element = HelpMethods.FindElement(driver, By.Id("input_26"), 4);
                element.SendKeys("36702763@clipsa.net");
                element.SendKeys(Keys.Enter);
                IWebElement element2 = HelpMethods.FindElement(driver, By.LinkText("кума"), 5);
                Assert.IsTrue(element2.Displayed);
                driver.Close();
                driver.Quit();
            }
            catch (Exception e)
            {
                driver.Close();
                driver.Quit();
                throw;
            }
        }

        [Test]
        public void SearchByIncorrectEmail()//поиск по некорректному email
        {
            IWebDriver driver = new ChromeDriver();
            try
            {

                HelpMethods.Authorization(CrmConstants.validLogin, CrmConstants.validPassword, driver);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                driver.Navigate().Refresh();
                IWebElement element = HelpMethods.FindElement(driver, By.Id("input_26"), 4);
                element.SendKeys("79897777113");
                element.SendKeys(Keys.Enter);
                // wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));


                string Text = "По Вашему запросу ничего не найдено";
                Thread.Sleep(5000);
                var elementText = HelpMethods.FindElementWithText(driver, By.ClassName("table__cell"), Text, 3);

                Assert.IsTrue(elementText.Displayed);
                driver.Close();
                driver.Quit();
            }
            catch (Exception e)
            {
                driver.Close();
                driver.Quit();
                throw;
            }
        }

        [Test]

        public void FilterByStatus()//фильтр по статусу
        {
            IWebDriver driver = new ChromeDriver();
            try
            {
                HelpMethods.Authorization(CrmConstants.validLogin, CrmConstants.validPassword, driver);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                driver.Navigate().Refresh();
                HelpMethods.FindElementAndClick(driver, By.Id("select_value_label_5"), 4);
                IWebElement element = HelpMethods.FindElement(driver, By.Id("select_container_23"), 5);
                string textStatus = "В работе";
                HelpMethods.FindElementWithTextAndClick(element, By.ClassName("ng-scope"), textStatus, 1);//выбран статус "Закрыт"
                var TableBody = HelpMethods.FindElement(driver, By.ClassName("table__body"), 5);
                var countRaws = HelpMethods.NumberOfRaws(TableBody, By.ClassName("ng-scope"), 1);
                var countElements = HelpMethods.NumberOfElements(driver, By.ClassName("table__cell"), textStatus, 5);
                if (countElements == countRaws)
                {
                    Assert.IsTrue(true);
                    driver.Close();
                    driver.Quit();
                }
                else
                {
                    Assert.IsTrue(false);
                    driver.Close();
                    driver.Quit();
                    
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                driver.Close();
                driver.Quit();
                throw;
            }
        }

        [Test]
        public void FilterByCategory()//фильтр по категории
        {
            IWebDriver driver = new ChromeDriver();
            try
            {
                HelpMethods.Authorization(CrmConstants.validLogin, CrmConstants.validPassword, driver);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                driver.Navigate().Refresh();
                HelpMethods.FindElementAndClick(driver, By.Id("select_value_label_4"), 4);
                IWebElement element1 = HelpMethods.FindElement(driver, By.Id("select_container_20"), 5);
                string textCategory = "43";
                HelpMethods.FindElementWithTextAndClick(element1, By.ClassName("ng-scope"), textCategory, 1);//выбрана категория "43"
                var countRaws = HelpMethods.NumberOfRaws(driver, By.ClassName("table__row_status-danger"), 5);
                var countElements = HelpMethods.NumberOfElements(driver, By.ClassName("table__cell"), textCategory, 5);
                if (countElements == countRaws)
                {
                    Assert.IsTrue(true);
                driver.Close();
                driver.Quit();
                    }
                

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                driver.Close();
                driver.Quit();
                throw;
            }
        }

        [Test]
        public void FilterByType()//фильтр по типу
        {
            IWebDriver driver = new ChromeDriver();
            try
            {
                HelpMethods.Authorization(CrmConstants.validLogin, CrmConstants.validPassword, driver);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                driver.Navigate().Refresh();
                HelpMethods.FindElementAndClick(driver, By.Id("select_value_label_3"), 4);
                IWebElement element2 = HelpMethods.FindElement(driver, By.Id("select_container_17"), 5);
                string textType = "Клиент";
                HelpMethods.FindElementWithTextAndClick(element2, By.ClassName("ng-scope"), textType, 1);//выбран Тип "Клиент"
                Thread.Sleep(3000);
                var TableBody = HelpMethods.FindElement(driver, By.ClassName("table__body"), 5);
                var countRaws = HelpMethods.NumberOfRaws(TableBody, By.ClassName("ng-scope"), 1);
                var countElements = HelpMethods.NumberOfElements(driver, By.ClassName("table__cell"), textType, 5);
                if (countElements == countRaws)
                {
                    Assert.IsTrue(true);
                    driver.Close();
                    driver.Quit();
                }
                else
                {
                    Assert.IsTrue(false);
                    driver.Close();
                    driver.Quit();
                }
                }
            catch (Exception e)
            {
                driver.Close();
                driver.Quit();
                Console.WriteLine(e);
                
                throw;
            }
        }
        [Test]
        public void FilterByOwner()//фильтр по владельцу
        {
            IWebDriver driver = new ChromeDriver();
            try
            {
                HelpMethods.Authorization(CrmConstants.validLogin, CrmConstants.validPassword, driver);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                driver.Navigate().Refresh();
                HelpMethods.FindElementAndClick(driver, By.Id("select_value_label_2"), 4);
                var element = HelpMethods.FindElement(driver, By.Id("input_12"), 1);
                string textOwner = "труба";
                element.SendKeys(textOwner);
                IWebElement element2 = HelpMethods.FindElement(driver, By.Id("select_container_14"), 5);
                IWebElement selectOwner = HelpMethods.FindElementWithTextAndClick(element2, By.ClassName("ng-scope"), textOwner, 3);//выбран Владелец "труба"
                Thread.Sleep(5000);
                IWebElement element3 = HelpMethods.FindElement(driver, By.LinkText(textOwner), 1);
                Assert.IsTrue(element3.Displayed);
                driver.Close();
                driver.Quit();
            }
            catch (Exception e)
            {
                driver.Close();
                driver.Quit();
                Console.WriteLine(e);
                throw;
            }
        }
        [Test]
        public void FilterByResponsible()//фильтр по ответственному
        {
            IWebDriver driver = new ChromeDriver();
            try
            {
                HelpMethods.Authorization(CrmConstants.validLogin, CrmConstants.validPassword, driver);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                driver.Navigate().Refresh();
                HelpMethods.FindElementAndClick(driver, By.Id("select_value_label_1"), 4);
                IWebElement clickResponsibleElement = HelpMethods.FindElement(driver, By.Id("input_9"), 1);//поле для ввода Ответственного
                string Text = "5505";
                clickResponsibleElement.SendKeys(Text);
                IWebElement element = HelpMethods.FindElement(driver, By.Id("select_container_11"), 5);
                IWebElement searchByResponsible = HelpMethods.FindElementWithText(element, By.ClassName("ng-scope"), Text, 1);
                searchByResponsible.Click();
                Thread.Sleep(5000);
                IWebElement searchInTableElement = HelpMethods.FindElement(driver, By.LinkText("вольво"), 4);
                Assert.IsTrue(searchInTableElement.Displayed);
                driver.Close();
                driver.Quit();
            }
            catch (Exception e)
            {
                driver.Close();
                driver.Quit();
                Console.WriteLine(e);
                throw;
            }
        }

        [Test]
        public void FilterByManager()//фильтр по менеджеру
        {
            IWebDriver driver = new ChromeDriver();
            try
            {
                HelpMethods.Authorization(CrmConstants.validLogin, CrmConstants.validPassword, driver);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                driver.Navigate().Refresh();
                HelpMethods.FindElementAndClick(driver, By.Id("select_value_label_0"), 5);//1960
                IWebElement elementForText = HelpMethods.FindElement(driver, By.Id("input_6"), 1);
                string Text = "1960";
                string Text2 = "7006";
                elementForText.SendKeys(Text);
                IWebElement element6 = HelpMethods.FindElement(driver, By.Id("select_container_8"), 5);
                IWebElement selectManager = HelpMethods.FindElementWithText(element6, By.ClassName("ng-scope"), Text, 1);
                selectManager.Click();
                //IWebElement element5 = Methods.FindElement(driver, By.Id("select_option_1358"), 1);//7006
                //element1.SendKeys("труба");
                //element5.Click();

                elementForText.Clear();
                elementForText.SendKeys(Text2);
                selectManager = HelpMethods.FindElementWithText(element6, By.ClassName("ng-scope"), Text2, 1);
                selectManager.Click();
                //IWebElement element2 = Methods.FindElementWithText(driver, By.ClassName("ng-scope"), "5505", 1);
                //element2.Click();
                Thread.Sleep(5000);
                IWebElement element3 = HelpMethods.FindElement(driver, By.LinkText("артем_лог"), 4);
                IWebElement element4 = HelpMethods.FindElement(driver, By.LinkText("азбука"), 4);
                Assert.IsTrue(element3.Displayed);
                Assert.IsTrue(element4.Displayed);
                driver.Close();
                driver.Quit();
            }
            catch (Exception e)
            {
                driver.Close();
                driver.Quit();
                Console.WriteLine(e);
                throw;
            }
        }
        [Test]
        public void FiltersNotReset() //кейс-верхние фильтры не должны сбрасывать друг друга
            
        {
            IWebDriver driver = new ChromeDriver();
            try
            {
                HelpMethods.Authorization(CrmConstants.validLogin, CrmConstants.validPassword, driver);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                driver.Navigate().Refresh();

                HelpMethods.FindElementAndClick(driver, By.Id("select_value_label_5"), 4);
                IWebElement element = HelpMethods.FindElement(driver, By.Id("select_container_23"), 5);
                string textStatus = "Закрыт";
                HelpMethods.FindElementWithTextAndClick(element, By.ClassName("ng-scope"), textStatus, 1);//выбран статус "Закрыт"
                Thread.Sleep(2000);
                var findStatus = HelpMethods.FindElementWithText(driver, By.ClassName("md-select-value"), textStatus, 5);

                HelpMethods.FindElementAndClick(driver, By.Id("select_value_label_4"), 4);
                IWebElement element1 = HelpMethods.FindElement(driver, By.Id("select_container_20"), 5);
                string textCategory = "22";
                HelpMethods.FindElementWithTextAndClick(element1, By.ClassName("ng-scope"), textCategory, 1);//выбрана категория "22"
                var findCategory = HelpMethods.FindElementWithText(driver, By.ClassName("md-select-value"), textCategory, 5);
                Assert.IsTrue(findStatus.Displayed);

                HelpMethods.FindElementAndClick(driver, By.Id("select_value_label_3"), 4);
                IWebElement element2 = HelpMethods.FindElement(driver, By.Id("select_container_17"), 5);
                string textType = "Клиент";
                IWebElement selectType = HelpMethods.FindElementWithTextAndClick(element2, By.ClassName("ng-scope"), textType, 1);//выбран Тип "Клиент"
                var findType = HelpMethods.FindElementWithText(driver, By.ClassName("md-select-value"), textType, 5);
                Assert.IsTrue(findStatus.Displayed);
                Assert.IsTrue(findCategory.Displayed);




                HelpMethods.FindElementAndClick(driver, By.Id("select_value_label_2"), 4);
                IWebElement element3 = HelpMethods.FindElement(driver, By.Id("select_container_14"), 5);
                string textOwner = "всх";
                IWebElement sendTextOwner = driver.FindElement(By.CssSelector("[placeholder='Поиск владельца']"));
                sendTextOwner.SendKeys(textOwner);
                IWebElement selectOwner = HelpMethods.FindElementWithTextAndClick(element3, By.ClassName("ng-scope"), textOwner, 3);//выбран Владелец "всх"
                var findOwner = HelpMethods.FindElementWithText(driver, By.ClassName("md-select-value"), textOwner, 5);
                Assert.IsTrue(findStatus.Displayed);
                Assert.IsTrue(findCategory.Displayed);
                Assert.IsTrue(findType.Displayed);





                HelpMethods.FindElementAndClick(driver, By.Id("select_value_label_1"), 4);
                IWebElement element4 = HelpMethods.FindElement(driver, By.Id("select_container_11"), 5);
                string textResponsible = "1101";
                IWebElement sendTextResponsible = driver.FindElement(By.CssSelector("[placeholder='Поиск ответственного']"));
                sendTextResponsible.SendKeys(textResponsible);
                HelpMethods.FindElementWithTextAndClick(element4, By.ClassName("ng-scope"), textResponsible, 3);//выбран Ответственный "1101"
                var findResponsible = HelpMethods.FindElementWithText(driver, By.ClassName("md-select-value"), textResponsible, 5);
                Assert.IsTrue(findStatus.Displayed);
                Assert.IsTrue(findCategory.Displayed);
                Assert.IsTrue(findType.Displayed);
                Assert.IsTrue(findOwner.Displayed);





                HelpMethods.FindElementAndClick(driver, By.Id("select_value_label_0"), 5);//7207
                string textManager = "7207";
                IWebElement sendTextManager = driver.FindElement(By.CssSelector("[placeholder='Поиск менеджера']"));
                sendTextManager.SendKeys(textManager);
                IWebElement element6 = HelpMethods.FindElement(driver, By.Id("select_container_8"), 5);
                IWebElement element7 = HelpMethods.FindElementWithText(element6, By.ClassName("ng-scope"), textManager, 1);
                element7.Click();//нашли менеджера 7207
                Assert.IsTrue(findStatus.Displayed);
                Assert.IsTrue(findCategory.Displayed);
                Assert.IsTrue(findType.Displayed);
                Assert.IsTrue(findOwner.Displayed);
                Assert.IsTrue(findResponsible.Displayed);

                driver.Close();
                driver.Quit();

            }
            catch (Exception e)
            {
                driver.Close();
                driver.Quit();
                Console.WriteLine(e);
                throw;
            }
        }

    }

}
