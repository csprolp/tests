﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using crmtest.DriverHelper;
using Constants;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace crmtest
{
    class Applications
    {
        [Test]
        public void RedirectToApplications()
        {
            IWebDriver driver = new ChromeDriver();
            try
            {
                HelpMethods.Authorization(CrmConstants.validLogin, CrmConstants.validPassword, driver);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                HelpMethods.FindElementAndClick(driver, By.CssSelector("[aria-label='Меню']"));
                HelpMethods.FindElementAndClick(driver, By.CssSelector("[aria-label='Заявки']"));
                IWebElement waitTable =
                    wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("requests__table-container")));
                var header = HelpMethods.FindElementWithText(driver, By.ClassName("requests-header"), "Заявки", 1);
                Assert.IsTrue(waitTable.Displayed);
                Assert.IsTrue(header.Displayed);
                driver.Close();
                driver.Quit();
            }
            catch (Exception e)
            {
                driver.Close();
                driver.Quit();
                Console.WriteLine(e);
                throw;
            }
        }


        [Test]
        public void ApplicationsInWork()
        {
            IWebDriver driver = new ChromeDriver();
            try
            {
                HelpMethods.Authorization(CrmConstants.validLogin, CrmConstants.validPassword, driver);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                HelpMethods.FindElementAndClick(driver, By.CssSelector("[aria-label='Меню']"));
                HelpMethods.FindElementAndClick(driver, By.CssSelector("[aria-label='Заявки']"));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("requests__table-container")));
                Thread.Sleep(3000);
                var plawka = HelpMethods.FindElementWithTextAndClick(driver, By.ClassName("md-tab"), "В РАБОТЕ", 1);
               // var inWork = driver.FindElement(By.ClassName("md-tab")).Text.Trim();
                

                    Assert.IsTrue(HelpMethods.FindElementWithText(driver, By.ClassName("md-active"), "В РАБОТЕ", 1).Displayed);
                driver.Close();
                driver.Quit();

            }
            catch (Exception e)
            {
                driver.Close();
                driver.Quit();
                Console.WriteLine(e);
                throw;
            }

        }

        [Test]
        public void ApplicationsPaid()
        {
            IWebDriver driver = new ChromeDriver();
            try
            {
                HelpMethods.Authorization(CrmConstants.validLogin, CrmConstants.validPassword, driver);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                HelpMethods.FindElementAndClick(driver, By.CssSelector("[aria-label='Меню']"));
                HelpMethods.FindElementAndClick(driver, By.CssSelector("[aria-label='Заявки']"));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("requests__table-container")));
                Thread.Sleep(3000);
                var plawka = HelpMethods.FindElementWithTextAndClick(driver, By.ClassName("md-tab"), "ОПЛАЧЕННЫЕ", 1);
                // var inWork = driver.FindElement(By.ClassName("md-tab")).Text.Trim();


                Assert.IsTrue(HelpMethods.FindElementWithText(driver, By.ClassName("md-active"), "ОПЛАЧЕННЫЕ", 1).Displayed);
                driver.Close();
                driver.Quit();

            }
            catch (Exception e)
            {
                driver.Close();
                driver.Quit();
                Console.WriteLine(e);
                throw;
            }

        }

        [Test]
        public void ApplicationsRefusals()
        {
            IWebDriver driver = new ChromeDriver();
            try
            {
                HelpMethods.Authorization(CrmConstants.validLogin, CrmConstants.validPassword, driver);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                HelpMethods.FindElementAndClick(driver, By.CssSelector("[aria-label='Меню']"));
                HelpMethods.FindElementAndClick(driver, By.CssSelector("[aria-label='Заявки']"));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("requests__table-container")));
                Thread.Sleep(3000);
                var plawka = HelpMethods.FindElementWithTextAndClick(driver, By.ClassName("md-tab"), "ОТКАЗАННЫЕ", 1);
                // var inWork = driver.FindElement(By.ClassName("md-tab")).Text.Trim();


                Assert.IsTrue(HelpMethods.FindElementWithText(driver, By.ClassName("md-active"), "ОТКАЗАННЫЕ", 1).Displayed);
                driver.Close();
                driver.Quit();

            }
            catch (Exception e)
            {
                driver.Close();
                driver.Quit();
                Console.WriteLine(e);
                throw;
            }

        }

        [Test]
        public void ApplicationsBlocked()
        {
            IWebDriver driver = new ChromeDriver();
            try
            {
                HelpMethods.Authorization(CrmConstants.validLogin, CrmConstants.validPassword, driver);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                HelpMethods.FindElementAndClick(driver, By.CssSelector("[aria-label='Меню']"));
                HelpMethods.FindElementAndClick(driver, By.CssSelector("[aria-label='Заявки']"));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("requests__table-container")));
                Thread.Sleep(3000);
                var plawka = HelpMethods.FindElementWithTextAndClick(driver, By.ClassName("md-tab"), "БЛОК", 1);
                // var inWork = driver.FindElement(By.ClassName("md-tab")).Text.Trim();


                Assert.IsTrue(HelpMethods.FindElementWithText(driver, By.ClassName("md-active"), "БЛОК", 1).Displayed);
                driver.Close();
                driver.Quit();

            }
            catch (Exception e)
            {
                driver.Close();
                driver.Quit();
                Console.WriteLine(e);
                throw;
            }

        }

        [Test]
        public void ApplicationsNew()
        {
            IWebDriver driver = new ChromeDriver();
            try
            {
                HelpMethods.Authorization(CrmConstants.validLogin, CrmConstants.validPassword, driver);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                HelpMethods.FindElementAndClick(driver, By.CssSelector("[aria-label='Меню']"));
                HelpMethods.FindElementAndClick(driver, By.CssSelector("[aria-label='Заявки']"));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("requests__table-container")));
                Thread.Sleep(3000);
                var plawka = HelpMethods.FindElementWithTextAndClick(driver, By.ClassName("md-tab"), "НОВЫЕ", 1);
                // var inWork = driver.FindElement(By.ClassName("md-tab")).Text.Trim();


                Assert.IsTrue(HelpMethods.FindElementWithText(driver, By.ClassName("md-active"), "НОВЫЕ", 1).Displayed);
                driver.Close();
                driver.Quit();

            }
            catch (Exception e)
            {
                driver.Close();
                driver.Quit();
                Console.WriteLine(e);
                throw;
            }

        }


    }
}
