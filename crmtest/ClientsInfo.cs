﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using crmtest.DriverHelper;
using Constants;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace crmtest
{
    class ClientsInfo
    {
        [Test]
        public void ClientsInfoMain()//поиск по коду клиента
        {
            IWebDriver driver = new ChromeDriver();
            try
            {

                HelpMethods.Authorization(CrmConstants.validLogin, CrmConstants.validPassword, driver);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                driver.Navigate().Refresh();
                IWebElement searchCode = HelpMethods.FindElement(driver, By.Id("input_24"), 4);
                string nameClient = "ТестМобСайт2";
                searchCode.SendKeys(nameClient);
                searchCode.SendKeys(Keys.Enter);
                HelpMethods.FindElementAndClick(driver, By.LinkText(nameClient), 5);
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-details-common__item")));
                Assert.IsTrue(HelpMethods.FindElementWithText(driver,By.ClassName("clients-details-common__item"),"Основное",1).Displayed);
                Assert.IsTrue(HelpMethods.FindElementWithText(driver, By.ClassName("clients-details-common__item"), "Прочее", 1).Displayed);
                Assert.IsTrue(HelpMethods.FindElementWithText(driver, By.ClassName("clients-details-common__item"), "Контакты", 1).Displayed);
                Assert.IsTrue(HelpMethods.FindElementWithText(driver, By.ClassName("md-active"), "ОБЩЕЕ", 1).Displayed);
                // Assert.IsTrue(findElement.Displayed);
                driver.Close();
                driver.Quit();
            }
            catch (Exception e)
            {
                driver.Close();
                driver.Quit();
                throw;
            }
        }

        //[Test]
        //public void IwiOwibki()

        //{
        //    IWebDriver driver = new ChromeDriver();
        //    try
        //    {
        //        while (driver.)
        //        {
                    
        //        }


        //        //if (HelpMethods.FindElement(driver, By.ClassName("md-toast-open-top"), 1).Displayed == true)
        //        //{
        //        //    Assert.IsTrue(false);
        //        //}
        //    }
        //    catch (Exception e)
        //    {
        //        driver.Close();
        //        driver.Quit();
        //        Console.WriteLine(e);
        //        throw;
        //    }
        //}
    }
}
