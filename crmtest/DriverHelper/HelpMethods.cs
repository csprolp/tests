﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Constants;

namespace crmtest.DriverHelper
{
    public class HelpMethods
    {
        private const int FindElementTimeOut = 15; //sec


        public static IWebDriver Authorization(string login, string password, IWebDriver driver)
        {
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(CrmConstants.addressDev);
            IWebElement loginElement = driver.FindElement(By.Id("input_0")); //поиск строки для ввода логина
            loginElement.SendKeys(login); //логин
            IWebElement passwordElement = driver.FindElement(By.Id("input_1")); //поиск строки для ввода пароля
            passwordElement.SendKeys(password); //пароль
            IWebElement element3 = driver.FindElement(By.ClassName("md-raised")); //поиск кнопки
            element3.Submit(); //нажатие на кнопку
            //var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
            return driver;


        }



        public static IWebElement FindElement(IWebDriver driver, By by, int timeout = FindElementTimeOut)
        {
            try
            {
                var element = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout)).Until(ExpectedConditions.ElementIsVisible(by));
                return element;
            }
            catch
            {
                return null;
            }
        }

        //public static IWebElement FindElements(IWebDriver driver, By by, int timeout = FindElementTimeOut)
        //{

        //        var elements = driver.FindElements(by);

        //        if (elements == null)
        //        {
        //            return null;
        //        }
        //        return elements;
            

        //}

        public static bool FindElementAndClick(IWebDriver driver, By by, int timeout = FindElementTimeOut)
        {
            try
            {
                var element = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout)).Until(ExpectedConditions.ElementIsVisible(by));
                element.Click();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static IWebElement FindElementAndClickReturnElement(IWebDriver driver, By by, int timeout = FindElementTimeOut)
        {
            try
            {
                var element = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout)).Until(ExpectedConditions.ElementIsVisible(by));
                element.Click();
                return element;
            }
            catch
            {
                return null;
            }
        }
        //public static bool FindElementAndClick(IWebElement driver, By by, int timeout = FindElementTimeOut)
        //{
        //    try
        //    {
        //        Thread.Sleep(5000);
        //        var element = driver.FindElements(by);
        //       // var element = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout)).Until(ExpectedConditions.ElementIsVisible(by));
        //        element.Click();
        //        return true;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}


        public static IWebElement FindElementWithText(IWebDriver driver, By by, string containedText, int timeout = FindElementTimeOut)
        {
            var elements = driver.FindElements(by);
            var elem = elements.FirstOrDefault(e => e.Text.Contains(containedText));
            if (elem == null)
            {
                return null;
            }
            return elem;
        }

        public static IWebElement FindElementWithText(IWebElement driver, By by, string containedText, int timeout = FindElementTimeOut)
        {
            var elements = driver.FindElements(by);
            var elem = elements.FirstOrDefault(e => e.Text.Contains(containedText));
            if (elem == null)
            {
                return null;
            }
            return elem;
        }

        public static IWebElement FindElementWithTextAndClick(IWebDriver driver, By by, string containedText, int timeout = FindElementTimeOut)
        {
            var elements = driver.FindElements(by);
           // elements = elements[].Text.Trim();
            var elem = elements.FirstOrDefault(e => e.Text.Contains(containedText));
            if (elem == null)
            {
                return null;
            }
            elem.Click();
            return elem;
        }


        public static IWebElement FindElementWithTextAndClick(IWebElement driver, By by, string containedText, int timeout = FindElementTimeOut)
        {
            var elements = driver.FindElements(by);
          //  elements = elements.Replace(" ", string.Empty);
            var elem = elements.FirstOrDefault(e => e.Text.Contains(containedText));
            if (elem == null)
            {
                return null;
            }
            elem.Click();
            return elem;
        }


        protected static IWebElement FindNullableElement(IWebDriver driver, By by)
        {
            try
            {
                return driver.FindElement(by);
            }
            catch
            {
                return null;
            }
        }

        public static ReadOnlyCollection<IWebElement> FindNullableElements(IWebDriver driver, By by)
        {
            try
            {
                return driver.FindElements(by);
            }
            catch
            {
                return null;
            }
        }
        public static IWebElement FindElementWithAnotherText(IWebDriver driver, By by, string containedText, int timeout = FindElementTimeOut)
        {
            var elements = driver.FindElements(by);
            var elem = elements.FirstOrDefault(e => e.Text.Contains(containedText));
            if (elem == null)
            {
                return null;
            }
            return elem;
        }

        public static IWebElement CheckElementsWithText(IWebElement driver, By by, string containedText, int timeout = FindElementTimeOut)
        {
            var elements = driver.FindElements(by);
            var elem = elements.FirstOrDefault(e => e.Text.Contains(containedText));
            if (elem == null)
            {
                return null;
            }
            return elem;
        }
        public static int NumberOfRaws(IWebDriver driver, By by, int timeout = FindElementTimeOut)
        {
            var elements = driver.FindElements(by);
            
            var elem = elements.Count;

            return elem;
        }

        public static int NumberOfRaws(IWebElement driver, By by, int timeout = FindElementTimeOut)
        {
            var elements = driver.FindElements(by);

            var elem = elements.Count;

            return elem;
        }

        public static int NumberOfElements(IWebDriver driver, By by,string containedText, int timeout = FindElementTimeOut)
        {
            var elements = driver.FindElements(by);

            var elem = elements.Count(e => e.Text.Contains(containedText));
            

            return elem;
        }

        public static void ScrollToBottom(IWebDriver driver)
        {
            long scrollHeigth = 0;
            do
            {
                IJavaScriptExecutor js = (IJavaScriptExecutor) driver;
                var newScrollHeight = (long) js.ExecuteScript("window.scrollTo(0,document.body.scrollHeight); return document.body.scrollHeight;");

                if (newScrollHeight == scrollHeigth)
                {
                    break;
                }
                else
                {
                    scrollHeigth = newScrollHeight;
                    Thread.Sleep(400);
                }
            } while (true);
        }
        
    }
}

