﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BasicTest;
using crmtest.DriverHelper;
using Constants;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace crmtest
{
    public class Administration 
    {
        [Test]
        public void RedirectToAdministration()
        {
          //  var baseTest = new BaseTest();
            IWebDriver driver = new ChromeDriver();
            try
            {
                HelpMethods.Authorization(CrmConstants.validLogin, CrmConstants.validPassword, driver);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                BaseTest.FindElementAndClick(driver, By.CssSelector("[aria-label='Меню']"),1);
                BaseTest.FindElementAndClick(driver, By.CssSelector("[aria-label='Администрирование']"),1);
                var groups = BaseTest.FindElement(driver, By.ClassName("notifications__table"), 7);
                var header = HelpMethods.FindElementWithText(driver, By.ClassName("header__title"), "Администрирование", 1);
                
                Assert.IsTrue(groups.Displayed && header.Displayed );
                driver.Close();
                driver.Quit();
            }
            catch (Exception e)
            {
                driver.Close();
                driver.Quit();
                Console.WriteLine(e);
                throw;
            }
        }
        [Test]
        public void RestrictionOfEnteredCharacters()
        {
           // var baseTest = new BaseTest();
            IWebDriver driver = new ChromeDriver();
            try
            {
                HelpMethods.Authorization(CrmConstants.validLogin, CrmConstants.validPassword, driver);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                BaseTest.FindElementAndClick(driver, By.CssSelector("[aria-label='Меню']"), 1);
                BaseTest.FindElementAndClick(driver, By.CssSelector("[aria-label='Администрирование']"), 1);
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("admin-access__details")));

              //  var test = HelpMethods.FindNullableElements(driver, By.CssSelector("[aria-activedescendant='tab - item - 43']"));
                    HelpMethods.FindElementWithTextAndClick(driver, By.CssSelector("[aria-activedescendant='tab - item - 43']"), "КЛИЕНТЫ", 1);
                //test.Click();
                HelpMethods.FindElementWithTextAndClick(driver, By.ClassName("md-button_small"), "СОЗДАТЬ КЛИЕНТА", 1);
                HelpMethods.FindElementAndClick(driver, By.CssSelector("[aria-label='Выбрать физ.лицо']"));
                var foundByCode =
                    HelpMethods.FindElementAndClickReturnElement(driver, By.CssSelector("[placeholder='Искать по коду']"), 1);
                foundByCode.SendKeys("63634634634634634");
                if (foundByCode.Text == "63634634634634634")
                {
                    Assert.IsTrue(false);
                }
                Assert.IsTrue(foundByCode.Text == "6364");
                driver.Close();
                driver.Quit();
            }
            catch (Exception e)
            {
                driver.Close();
                driver.Quit();
                Console.WriteLine(e);
                throw;
            }
        }
    }
    
}
