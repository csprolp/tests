﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BasicTest;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using crmtest.DriverHelper;
using Constants;
using NUnit.Framework;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace crmtest
{
    public class Alerts : BaseTest
    { [Test]
        public void RedirectToAlerts()
        {
            IWebDriver driver = new ChromeDriver();
            try
            {
                HelpMethods.Authorization(CrmConstants.validLogin, CrmConstants.validPassword, driver);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                FindElementAndClick(driver, By.CssSelector("[aria-label='Меню']"));
                FindElementAndClick(driver, By.CssSelector("[aria-label='Оповещения']"),1);
                var waitTable = FindElement(driver, By.ClassName("notifications__table"), 7);
                var header = FindElementWithText(driver, By.ClassName("header__title"), "Обращения", 1);
                Assert.IsTrue(waitTable.Displayed);
                driver.Close();
                driver.Quit();
            }
            catch (Exception e)
            {
                driver.Close();
                driver.Quit();
                Console.WriteLine(e);
                throw;
            }
        }

        [Test]
        public void OpenMenuByDate()//открытие меню Получено
        {
            {
                IWebDriver driver = new ChromeDriver();
                try
                {

                    HelpMethods.Authorization(CrmConstants.validLogin, CrmConstants.validPassword, driver);
                    var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                    wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                    FindElementAndClick(driver, By.CssSelector("[aria-label='Меню']"), 2);
                    FindElementAndClick(driver, By.CssSelector("[aria-label='Оповещения']"), 2);
                    Thread.Sleep(5000);
                    wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("notifications__table")));
                    string Find = "Получено";
                    HelpMethods.FindElementWithTextAndClick(driver, By.ClassName("table__cell"), Find, 2);
                    var OpenFilter = HelpMethods.FindElementWithText(driver, By.ClassName("md-button"), "Открыть фильтр", 5);
                    Assert.IsTrue(OpenFilter.Displayed);
                    driver.Close();
                    driver.Quit();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    driver.Close();
                    driver.Quit();
                    throw;
                }
            }
        }

        [Test]
        public void OpenFilterByDate()//открытие фильтров Получено
        {
            {
                IWebDriver driver = new ChromeDriver();
                try
                {

                    HelpMethods.Authorization(CrmConstants.validLogin, CrmConstants.validPassword, driver);
                    var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                    wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                    HelpMethods.FindElementAndClick(driver, By.CssSelector("[aria-label='Меню']"), 2);
                    HelpMethods.FindElementAndClick(driver, By.CssSelector("[aria-label='Оповещения']"), 2);
                    Thread.Sleep(5000);
                    wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("notifications__table")));
                    string Find = "Получено";
                    HelpMethods.FindElementWithTextAndClick(driver, By.ClassName("table__cell"), Find, 2);
                    HelpMethods.FindElementWithTextAndClick(driver, By.ClassName("md-button"), "Открыть фильтр", 5);
                    var FilterByDate = HelpMethods.FindElement(driver, By.ClassName("n-grid-filter-date"), 2);
                    Assert.IsTrue(FilterByDate.Displayed);
                    driver.Close();
                    driver.Quit();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    driver.Close();
                    driver.Quit();
                    throw;
                }
            }
        }



        [Test]
        public void FilterByDateAlerts()
        {
            IWebDriver driver = new ChromeDriver();
            try
            {
                HelpMethods.Authorization(CrmConstants.validLogin, CrmConstants.validPassword, driver);
                var wait = new WebDriverWait(driver,TimeSpan.FromSeconds(5));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                HelpMethods.FindElementAndClick(driver, By.CssSelector("[aria-label='Меню']"),2);
                HelpMethods.FindElementAndClick(driver, By.CssSelector("[aria-label='Оповещения']"),2);
                Thread.Sleep(5000);
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("notifications__table")));
                string Find = "Получено";
                HelpMethods.FindElementWithTextAndClick(driver, By.ClassName("table__cell"),Find,2);
                HelpMethods.FindElementWithTextAndClick(driver, By.ClassName("md-button"), "Открыть фильтр", 5);
                string text = "Начало периода";
                var dateFrom = HelpMethods.FindElementWithTextAndClick(driver, By.ClassName("md-block"), text, 1);
                var dateField = HelpMethods.FindElement(driver,By.ClassName("md-datepicker-input-mask-opaque"),2);
                //dateFrom.Click();
                dateField.SendKeys("29.06.2017");
                HelpMethods.FindElementAndClick(driver, By.ClassName("n-grid-filter-date__title"), 1);
                string text2 = "Конец периода";
                HelpMethods.FindElementWithTextAndClick(driver, By.ClassName("form__item"), text2, 1);
                var dateTo = HelpMethods.FindElementAndClickReturnElement(driver, By.ClassName("md-datepicker-input-mask"), 1);
                dateTo.SendKeys("02.07.2017");
                HelpMethods.FindElementAndClick(driver, By.ClassName("header__title"), 1);
                var name = "21915526";
                HelpMethods.FindElementWithTextAndClick(driver, By.ClassName("table__cell"), name, 1);
                HelpMethods.ScrollToBottom(driver);
                var textDate = "29.06.2017";
                var TableBody = HelpMethods.FindElement(driver, By.ClassName("notifications__table"), 1);
                var countRaws = HelpMethods.NumberOfRaws(TableBody, By.ClassName("ng-scope"), 1);
                var countElements = HelpMethods.NumberOfElements(driver, By.ClassName("notifications__table"), textDate, 5);
                if (countElements == countRaws)
                {
                    Assert.IsTrue(true);
                    driver.Close();
                    driver.Quit();
                }
                else
                {
                    Assert.IsTrue(false);

                }
            }
            
            catch (Exception e)
            {
                driver.Close();
                driver.Quit();
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
