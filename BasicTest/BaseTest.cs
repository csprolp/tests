﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace BasicTest
{
    public class BaseTest
    {
        private const int FindElementTimeOut = 15; //sec

        
        public static IWebElement FindElement(IWebDriver driver, By by, int timeout = FindElementTimeOut)
        {
            try
            {
                var element = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout)).Until(ExpectedConditions.ElementIsVisible(by));
                return element;
            }
            catch
            {
                return null;
            }
        }

        public static bool FindElementAndClick(IWebDriver driver, By by, int timeout = FindElementTimeOut)
        {
            try
            {
                var element = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout)).Until(ExpectedConditions.ElementIsVisible(by));
                element.Click();
                return true;
            }
            catch
            {
                return false;
            }
        }

        protected IWebElement FindElementAndClickReturnElement(IWebDriver driver, By by, int timeout = FindElementTimeOut)
        {
            try
            {
                var element = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout)).Until(ExpectedConditions.ElementIsVisible(by));
                element.Click();
                return element;
            }
            catch
            {
                return null;
            }
        }
        //public static bool FindElementAndClick(IWebElement driver, By by, int timeout = FindElementTimeOut)
        //{
        //    try
        //    {
        //        Thread.Sleep(5000);
        //        var element = driver.FindElements(by);
        //       // var element = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout)).Until(ExpectedConditions.ElementIsVisible(by));
        //        element.Click();
        //        return true;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}


        protected IWebElement FindElementWithText(IWebDriver driver, By by, string containedText, int timeout = FindElementTimeOut)
        {
            var elements = driver.FindElements(by);
            var elem = elements.FirstOrDefault(e => e.Text.Contains(containedText));
            if (elem == null)
            {
                return null;
            }
            return elem;
        }

        protected IWebElement FindElementWithText(IWebElement driver, By by, string containedText, int timeout = FindElementTimeOut)
        {
            var elements = driver.FindElements(by);
            var elem = elements.FirstOrDefault(e => e.Text.Contains(containedText));
            if (elem == null)
            {
                return null;
            }
            return elem;
        }

        protected IWebElement FindElementWithTextAndClick(IWebDriver driver, By by, string containedText, int timeout = FindElementTimeOut)
        {
            var elements = driver.FindElements(by);
            var elem = elements.FirstOrDefault(e => e.Text.Contains(containedText));
            if (elem == null)
            {
                return null;
            }
            elem.Click();
            return elem;
        }


        protected IWebElement FindElementWithTextAndClick(IWebElement driver, By by, string containedText, int timeout = FindElementTimeOut)
        {
            var elements = driver.FindElements(by);
            var elem = elements.FirstOrDefault(e => e.Text.Contains(containedText));
            if (elem == null)
            {
                return null;
            }
            elem.Click();
            return elem;
        }


        protected IWebElement FindNullableElement(IWebDriver driver, By by)
        {
            try
            {
                return driver.FindElement(by);
            }
            catch
            {
                return null;
            }
        }

        protected ReadOnlyCollection<IWebElement> FindNullableElements(IWebDriver driver, By by)
        {
            try
            {
                return driver.FindElements(by);
            }
            catch
            {
                return null;
            }
        }
        protected IWebElement FindElementWithAnotherText(IWebDriver driver, By by, string containedText, int timeout = FindElementTimeOut)
        {
            var elements = driver.FindElements(by);
            var elem = elements.FirstOrDefault(e => e.Text.Contains(containedText));
            if (elem == null)
            {
                return null;
            }
            return elem;
        }

        protected IWebElement CheckElementsWithText(IWebElement driver, By by, string containedText, int timeout = FindElementTimeOut)
        {
            var elements = driver.FindElements(by);
            var elem = elements.FirstOrDefault(e => e.Text.Contains(containedText));
            if (elem == null)
            {
                return null;
            }
            return elem;
        }
        public int NumberOfRaws(IWebDriver driver, By by, int timeout = FindElementTimeOut)
        {
            var elements = driver.FindElements(by);

            var elem = elements.Count;

            return elem;
        }

        protected int NumberOfRaws(IWebElement driver, By by, int timeout = FindElementTimeOut)
        {
            var elements = driver.FindElements(by);

            var elem = elements.Count;

            return elem;
        }

        protected int NumberOfElements(IWebDriver driver, By by, string containedText, int timeout = FindElementTimeOut)
        {
            var elements = driver.FindElements(by);

            var elem = elements.Count(e => e.Text.Contains(containedText));


            return elem;
        }

        protected void ScrollToBottom(IWebDriver driver)
        {
            long scrollHeigth = 0;
            do
            {
                IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                var newScrollHeight = (long)js.ExecuteScript("window.scrollTo(0,document.body.scrollHeight); return document.body.scrollHeight;");

                if (newScrollHeight == scrollHeigth)
                {
                    break;
                }
                else
                {
                    scrollHeigth = newScrollHeight;
                    Thread.Sleep(400);
                }
            } while (true);
        }
    }
}
