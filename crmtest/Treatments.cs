﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using crmtest.DriverHelper;
using Constants;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;

namespace crmtest
{
    class Treatments//Обращения
    {
        [Test]
        public void RedirectToTreatments()
        {
            IWebDriver driver = new ChromeDriver();
            try
            {
                HelpMethods.Authorization(CrmConstants.validLogin,CrmConstants.validPassword,driver);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                HelpMethods.FindElementAndClick(driver,By.CssSelector("[aria-label='Меню']"));
                HelpMethods.FindElementAndClick(driver, By.CssSelector("[aria-label='Обращения']"));
                IWebElement waitTable =
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("helpdesk-board__columns")));
                Assert.IsTrue(waitTable.Displayed);
                driver.Close();
                driver.Quit();
            }
            catch (Exception e)
            {
                driver.Close();
                driver.Quit();
                Console.WriteLine(e);
                throw;
            }
        }

        [Test]
        public void openTreatment()
        {
            IWebDriver driver = new ChromeDriver();
            
            try
            {
                HelpMethods.Authorization(CrmConstants.validLogin, CrmConstants.validPassword, driver);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                HelpMethods.FindElementAndClick(driver, By.CssSelector("[aria-label='Меню']"));
                HelpMethods.FindElementAndClick(driver, By.CssSelector("[aria-label='Обращения']"));
                IWebElement waitTable =
                    wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("helpdesk-board__columns")));
                IWebElement numberOfTreatmentHelper =
                    driver.FindElement(By.ClassName("helpdesk-board-request__number"));
                var element =
                   numberOfTreatmentHelper.FindElements(By.ClassName("ng-binding"));
                var elem = element.First();
                elem.Click();
                IWebElement checkElement = wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("helpdesk-details-wrap__content")));
                Assert.IsTrue(checkElement.Displayed);
                driver.Close();
                driver.Quit();

            }
            catch (Exception e)
            {
                driver.Close();
                driver.Quit();
                throw;
            }
        }


        [Test]
        public void sendMessageInTreatment()
        {
            IWebDriver driver = new ChromeDriver();

            try
            {
                HelpMethods.Authorization(CrmConstants.validLogin, CrmConstants.validPassword, driver);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
                wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("clients-list__table")));
                HelpMethods.FindElementAndClick(driver, By.CssSelector("[aria-label='Меню']"));
                HelpMethods.FindElementAndClick(driver, By.CssSelector("[aria-label='Обращения']"));
                IWebElement waitTable =
                    wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("helpdesk-board__columns")));
                IWebElement numberOfTreatmentHelper =
                    driver.FindElement(By.ClassName("helpdesk-board-request__number"));
                var element =
                    numberOfTreatmentHelper.FindElements(By.ClassName("ng-binding"));
                var elem = element.First();
                elem.Click();
                var goToMessages= HelpMethods.FindElement(driver, By.CssSelector("li[aria-label='Сообщения']"), 5);
                goToMessages.Click();

                Thread.Sleep(TimeSpan.FromSeconds(2));
                string textMessage = "123Тест";
                var textArea = HelpMethods.FindElement(driver, By.ClassName("md-resize-wrapper"));

                var text = textArea.FindElement(By.TagName("textarea"));
                //textArea.Click();

                //Methods.FindElementAndClickReturnElement(driver, By.ClassName("helpdesk-details-im-compose__text"), 2);

                text.SendKeys(textMessage);
                
                HelpMethods.FindElementAndClick(driver, By.CssSelector("button[aria-label='Отправить сообщение']"), 1);
                var findMessage = HelpMethods.FindElementWithText(driver, By.ClassName("helpdesk-details-im-message__text"),
                    textMessage, 1);
                






                //IWebElement checkElement = wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("helpdesk-details-wrap__content")));
                Assert.IsTrue(findMessage.Displayed);
                driver.Close();
                driver.Quit();

            }
            catch (Exception e)
            {
                driver.Close();
                driver.Quit();
                throw;
            }
        }
    }
}
